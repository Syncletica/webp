﻿#include <fstream>
#include <memory>
#include <webp/encode.h>

using namespace std;

int main() {
    // Создание и инициализация буфера
    const auto height = 480;
    const auto width = 640;
    auto data = make_unique<uint8_t[]>(height * width * 3);
    for (auto i = 0; i < height * width * 3; i += 3) {
        if (i < height * width) {
            data[i] = 255;
            data[i + 1] = 255;
            data[i + 2] = 255;
        }
        else if (i < height * width * 2) {
            data[i] = 0;
            data[i + 1] = 0;
            data[i + 2] = 255;
        }
        else {
            data[i] = 255;
            data[i + 1] = 0;
            data[i + 2] = 0;
        }
    }

    // Создание изображения
    WebPPicture picture;
    if (!WebPPictureInit(&picture)) {
        return EXIT_FAILURE;
    }
    picture.width = 640;
    picture.height = 480;
    WebPPictureImportRGB(&picture, data.get(), width * 3);
    WebPMemoryWriter writer;
    WebPMemoryWriterInit(&writer);
    picture.writer = WebPMemoryWrite;
    picture.custom_ptr = &writer;

    // Инициализация конфигурации по умолчанию
    WebPConfig config;
    if (!WebPConfigPreset(&config, WEBP_PRESET_DEFAULT, 100)) {
        return EXIT_FAILURE;
    }

    // Ручная конфигурация параметров
    config.use_sharp_yuv = 1;

    // Кодирование
    const auto ok = WebPEncode(&config, &picture);
    WebPPictureFree(&picture);
    if (!ok) {
        return picture.error_code;
    }
    {
        fstream f("test.webp", fstream::binary | fstream::out);
        f.write(reinterpret_cast<char*>(writer.mem), writer.size);
    }
    printf("READY!");
    getchar();


    //uint8_t *output = NULL;
    //size_t size = WebPEncodeRGB(data.get(), width, height, width * 3, 100, &output);
    //{
    //	fstream f("test.webp", fstream::binary | fstream::out);
    //	f.write(reinterpret_cast<char*>(output), size);
    //}
    //WebPFree(output);
    //printf("READY!");
    //getchar();
}
