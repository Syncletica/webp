@echo off
set count=0
for %%x in (*.webp) do (
    set /a count+=1
)
if %count%==1 goto auto
set /p myfile=Enter file name: 
vwebp %myfile%
goto exit;
:auto
for %%x in (*.webp) do vwebp %%x
:exit